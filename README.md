# Proyecto2 - Arquitectura Computacional 1
# USAC - Guatemala


## especificación técnica 

* Realizado en Windows 10 
<div>
<p style = 'text-align:center;'>
<img src="./img/SO.PNG" alt="JuveYell" width="600px">
</p>
</div>

* Versión de DosBox
<div>
<p style = 'text-align:center;'>
<img src="./img/dosbox-v.png" alt="JuveYell" width="600px">
</p>
</div>


# ------------------------------------------------------------------
# ------------------------------------------------------------------
# ------------------------------------------------------------------
# ------------------------------------------------------------------


* Ejecución e interfaz principal del programa 
<div>
<p style = 'text-align:center;'>
<img src="./img/2.png" alt="JuveYell" width="600px">
</p>
</div>

* Opción 1 - Sub menu de la opción 1
<div>
<p style = 'text-align:center;'>
<img src="./img/2.1.png" alt="JuveYell" width="500px">
</p>
</div>

* Opción 1 - Ingreso de la ecuación (Máximo Grado 4, Mínimo Grado 0)
<div>
<p style = 'text-align:center;'>
<img src="./img/2.2.png" alt="JuveYell" width="400px">
</p>
</div>


* opción 2 - Visualización de la ecuación ingresada almacenada en memoria
<div>
<p style = 'text-align:center;'>
<img src="./img/3.png" alt="JuveYell" width="400px">
</p>
</div>

* Opción 3 - Derivada de la función en memoria
<div>
<p style = 'text-align:center;'>
<img src="./img/4.png" alt="JuveYell" width="400px">
</p>
</div>

* Opción 4 - Integración de la ecuación en memoria 
<div>
<p style = 'text-align:center;'>
<img src="./img/5.png" alt="JuveYell" width="400px">
</p>
</div>

* Opción 5 - SubMenú de las graficas de la ecuación y sus operaciones anteriores realizadas, y especificación del rango en coordenadas del eje "X"
<div>
<p style = 'text-align:center;'>
<img src="./img/7.png" alt="JuveYell" width="400px">
</p>
</div>

* Gráficas varias
<div>
<p style = 'text-align:center;'>
<img src="./img/8.png" alt="JuveYell" width="400px">
</p>
<p style = 'text-align:center;'>
<p><label> Grafica de segundo grado </label></p>
<img src="./img/9 segundo grado.png" alt="JuveYell" width="400px">
</p>
</div>

